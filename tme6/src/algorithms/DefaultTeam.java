package algorithms;

import java.awt.Point;
import java.util.ArrayList;

import supportGUI.Circle;
import supportGUI.Line;

public class DefaultTeam {

  // calculDiametre: ArrayList<Point> --> Line
  //   renvoie une pair de points de la liste, de distance maximum.
  public Line calculDiametre(ArrayList<Point> points) {
    if (points.size()<3) {
      return null;
    }

    Point p=points.get(0);
    Point q=points.get(1);

    /*******************
     * PARTIE A ECRIRE *
     *******************/
    return new Line(p,q);
  }

  // calculDiametreOptimise: ArrayList<Point> --> Line
  //   renvoie une pair de points de la liste, de distance maximum.
  public Line calculDiametreOptimise(ArrayList<Point> points) {
    if (points.size()<3) {
      return null;
    }

    Point p=points.get(1);
    Point q=points.get(2);

    /*******************
     * PARTIE A ECRIRE *
     *******************/
    return new Line(p,q);
  }

  // calculCercleMin: ArrayList<Point> --> Circle
  //   renvoie un cercle couvrant tout point de la liste, de rayon minimum.
  public Circle calculCercleMin(ArrayList<Point> points) {
    if (points.isEmpty()) {
      return null;
    }

    Point center=points.get(0);
    int radius=100;

    /*******************
     * PARTIE A ECRIRE *
     *******************/
    return new Circle(center,radius);
  }

  // enveloppeConvexe: ArrayList<Point> --> ArrayList<Point>
  //   renvoie l'enveloppe convexe de la liste.
  public ArrayList<Point> enveloppeConvexe(ArrayList<Point> points){
	
    if (points.size()<3) {
      return null;
    }

    ArrayList<Point> enveloppe = new ArrayList<Point>();
    
    //algo naif 
    /*try {
	    for(Point p : points)
	    {
	    	for(Point q : points)
	    	{
	    		if (p.equals(q))
	    			continue;
	    		
	    		boolean estcote = true;
	    		int i = 0; 
	    		while (crossProduct(p, q, points.get(i)) == 0)
		    		i++;
	    			double signRef = crossProduct (p,q,points.get(i));
		    			
	    			for(Point r : points)
	    			{
	    				
	    				if((crossProduct (p,q,r)*signRef) < 0)
							{
								estcote = false;
								break;
							}
	    			}
		    		
		    		if(estcote) {enveloppe.add(p);
					 enveloppe.add(q);}
		    	}
	    }
    }catch (Exception e) {
    	System.out.println(e);
    }*/
    
    //Tri pixel
    int xmax = 0;
    for (Point p : points) 
    	if (p.x > xmax)
    		xmax = p.x;
    
    Point[] ymin = new Point[xmax + 10];
    Point[] ymax = new Point[xmax + 10];
    
    for (Point p : points) {
    	if ( ymin[p.x] == null || ymin[p.x].y > p.y)
    		ymin[p.x] = p;
    	if ( ymax[p.x] == null || ymax[p.x].y < p.y)
    		ymax[p.x] = p;
    }
    
    //retire doublons successifs
    Point lastMin = null;
    Point firstMax = null;
    ArrayList<Point> parcours = new ArrayList<Point>();
    for (int i=0; i<ymin.length; i++)
    	if (ymin[i]!=null) {
    		parcours.add(ymin[i]);
    		lastMin = ymin[i];
    	}
    
    
    for (int i = ymax.length-1 ; i>=0; i--)
    	
    	if (ymax[i]!=null && !ymax[i].equals(lastMin)) {
    		parcours.add(ymax[i]);
    		firstMax = ymax[i];
    	}
    
    if (parcours.get(0).equals(firstMax))
    	parcours.remove(parcours.size()-1);
    
    enveloppe.add(parcours.get(0));
    Point p = null;
    Point q = null;
    Point r = null;
    int d, j = 0;
    try {
	    for (int i = 0; i<parcours.size(); i++) {
	    	p = parcours.get(i);
	    	q = parcours.get((i+1)%(parcours.size()-1));
	    	r = parcours.get((i+2)%(parcours.size()-1));
	    	d = crossProduct(p, r, q);
	    	
	    	if (d>0) {
	    		enveloppe.add(q);
	    		System.out.println("in");
	    	}
//	    	else {
//	    		while (d == 0) {
//		    		i++;
//	    			q = parcours.get((i+1)%(parcours.size()-1));
//	    	    	r = parcours.get((i+2)%(parcours.size()-1));
//	    	    	d = crossProduct(p, r, q);
//	    		}
//	    		if (d>0) 
//		    		enveloppe.add(q);
//	    	}
	    }
    }catch (Exception e) {
    	System.out.println(e);
    }
    
    return enveloppe;
  }
  
  public int crossProduct (Point A, Point B, Point P) {
	  return ((P.x - A.x)*(B.y - A.y)) - ((P.y - A.y)*(B.x - A.x));
  }

}
