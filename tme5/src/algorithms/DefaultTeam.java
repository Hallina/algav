package algorithms;

import java.awt.Point;
import java.util.ArrayList;

/***************************************************************
 * TME 1: calcul de diamètre et de cercle couvrant minimum.    *
 *   - Trouver deux points les plus éloignés d'un ensemble de  *
 *     points donné en entrée.                                 *
 *   - Couvrir l'ensemble de poitns donné en entrée par un     *
 *     cercle de rayon minimum.                                *
 *                                                             *
 * class Circle:                                               *
 *   - Circle(Point c, int r) constructs a new circle          *
 *     centered at c with radius r.                            *
 *   - Point getCenter() returns the center point.             *
 *   - int getRadius() returns the circle radius.              *
 *                                                             *
 * class Line:                                                 *
 *   - Line(Point p, Point q) constructs a new line            *
 *     starting at p ending at q.                              *
 *   - Point getP() returns one of the two end points.         *
 *   - Point getQ() returns the other end point.               *
 ***************************************************************/
import supportGUI.Circle;
import supportGUI.Line;

public class DefaultTeam {

	// calculDiametre: ArrayList<Point> --> Line
	// renvoie une paire de points de la liste, de distance maximum.
	public Line calculDiametre(ArrayList<Point> points) {
		if (points.size() < 3) {
			return null;
		}

		Point p = points.get(0);
		Point q = points.get(1);

		double dist = 0;
		double tmp;
		
		Point t1 = p;
		Point t2 = q;
		
		
		for (int i = 0; i<points.size(); i++) {
			t1 = points.get(i);
			for (int j = 1; j<points.size(); j++) {
				t2 = points.get(j);
				tmp = Math.sqrt((t1.y - t2.y) * (t1.y - t2.y) + (t1.x - t2.x) * (t1.x - t2.x));
				if (tmp > dist) {
					dist = tmp;
					p = t1;
					q = t2;
				}
			}
		}
		
		return new Line(p, q);
	}

	// calculCercleMin: ArrayList<Point> --> Circle
	// renvoie un cercle couvrant tout point de la liste, de rayon minimum.
	public Circle calculCercleMin(ArrayList<Point> points) {
		if (points.isEmpty()) {
			return null;
		}

		
		
		/* ******************* 
		 * Naïf 
		 * *******************/
		/*
	 	Point center = points.get(0);
		int radius = 500;
		for (Point p : points) {
			for (Point q : points) {
				center = new Point((p.x + q.x) / 2, ((p.y + q.y) / 2));
				radius = (int) Math.sqrt((q.y - p.y) * (q.y - p.y) + (q.x - p.x) * (q.x - p.x));
				boolean lbpq = true;
				for (Point r : points) {
					if ((r.y - center.y) * (r.y - center.y) + (r.x - center.x) * (r.x - center.x) > radius * radius) {
						lbpq = false;
						break;
					}
				}
				if (lbpq)
					return new Circle(center, radius);
			}
		}

	for (Point p : points) {
		for (Point q : points) {
			for (Point r : points) {
			}
		}
		}*/
		
		Point dummy = points.get(0);
		Point p = new Point();
		Point q = new Point();
		int dmax = 0;

		for (Point r : points) {
			int d = (int) Math.sqrt((r.y - dummy.y) * (r.y - dummy.y) + (r.x - dummy.x) * (r.x - dummy.x));
			if(d>dmax) {
				dmax = d;
				p = r;
			}
		}
		
		dmax=0;
		for (Point r : points) {
			int d = (int) Math.sqrt((r.y - p.y) * (r.y - p.y) + (r.x - p.x) * (r.x - p.x));
			if(d>dmax) {
				dmax = d;
				q = r;
			}
		}
		
		double cercleX, cercleY, cercleRayon;
		cercleX = (p.x+q.x)/2;
		cercleY = (p.y+q.y)/2;
		cercleRayon = dmax / 2;
		
		ArrayList<Point> copie = new ArrayList<Point>(points);
		copie.remove(p);
		copie.remove(q);
		
		Point s = new Point();
		double x =0;
		double y =0;
		while (!copie.isEmpty()) {
			s = copie.get(0);
			x = s.x -cercleX;
			y = s.y -cercleY;
			if ((x*x + y*y) <= cercleRayon * cercleRayon) {
				copie.remove(0);
			}
			else
			{
				double cs = Math.sqrt((s.y - cercleY) * (s.y - cercleY) + (s.x - cercleX) * (s.x - cercleX));
				double st = cs + cercleRayon;				
				cercleRayon  = st / 2;
				double cc2 = cs - cercleRayon;
				double alpha = cercleRayon / cs;
				double beta = cc2 / cs;
				cercleX = alpha * cercleX + beta * s.x;
				cercleY = alpha * cercleY + beta * s.y;
				copie.remove(s);
			}
		}
		
		Point c = new Point((int)cercleX, (int)cercleY);
		return new Circle(c,(int) cercleRayon);
	}
}
